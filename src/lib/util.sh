#!/bin/sh

CG_NAME=${CG_NAME:-@cgname@}

CGROUP_OPTS=nodev,noexec,nosuid

cgroup2_find_path() {
    if grep -qw cgroup2 /proc/filesystems; then
        printf "/sys/fs/cgroup"
    fi
    return 0
}

# {{{ mount-cgroups

cgroup2_base() {
    if ! grep -qw cgroup2 /proc/filesystems; then
        return 0
    fi

    local base
    base="$(cgroup2_find_path)"
    mkdir -p "${base}"

    if ! mount -t cgroup2 none -o "${CGROUP_OPTS},nsdelegate" "${base}" 2> /dev/null; then
        mount -t cgroup2 none -o "${CGROUP_OPTS}" "${base}"
    fi
    return 0
}

cgroup2_controllers() {
    if ! grep -qw cgroup2 /proc/filesystems; then
        return 0
    fi

    local active cgroup_path x
    cgroup_path="$(cgroup2_find_path)"

    if [ -z "${cgroup_path}" ]; then
        return 0
    fi

    if [ ! -e "${cgroup_path}/cgroup.controllers" ]; then
        return 0
    fi

    if [ ! -e "${cgroup_path}/cgroup.subtree_control" ]; then
        return 0
    fi

    read -r active < "${cgroup_path}/cgroup.controllers"
    for x in ${active}; do
        echo "+${x}"  > "${cgroup_path}/cgroup.subtree_control"
    done
    return 0
}

mount_cgroups() {
    if [ -d /sys/fs/cgroup ];then
        cgroup2_base
        cgroup2_controllers
        return 0
    fi
    return 1
}

# }}}

# {{{ sv cgroup setup

cgroup2_set_limits() {
    local cgroup_path

    cgroup_path="$(cgroup2_find_path)"

    if [ -z "${cgroup_path}" ]; then
        return 0
    fi

    if ! mountpoint -q "${cgroup_path}"; then
        return 0
    fi

    sv_cgroup_path="${cgroup_path}/${SV_CG}"

    if [ ! -d "${sv_cgroup_path}" ]; then
        mkdir "${sv_cgroup_path}"
    fi

    if [ -f "${sv_cgroup_path}"/cgroup.procs ]; then
        printf 0 > "${sv_cgroup_path}"/cgroup.procs
    fi

    if [ -z "${SV_CG_SETTINGS}" ]; then
        return 0
    fi

    while read -r key value; do
        [ -z "${key}" ] && continue
        [ -z "${value}" ] && continue
        [ ! -f "${sv_cgroup_path}/${key}" ] && continue
        printf "%s" "${value}" > "${sv_cgroup_path}/${key}"
    done < ${SV_CG_SETTINGS}
    return 0
}

sv_cgroup2_setup() {
    if grep -qs /sys/fs/cgroup /proc/1/mountinfo; then
        if [ -d /sys/fs/cgroup -a ! -w /sys/fs/cgroup ]; then
            break
        fi
    fi

    for d in /sys/fs/cgroup/* ; do
        [ -w "${d}"/tasks ] && printf "%d" 0 > "${d}"/tasks
    done

    cgroup2_set_limits
}

# }}}

# {{{ sv cgroup cleanup

cgroup2_running() {
    [ -d "/sys/fs/cgroup/${SV_NAME}" ]
}

cgroup2_get_pids() {
    local cgroup_procs p
    cgroup_pids=
    cgroup_procs="$(cgroup2_find_path)"
    if [ -n "${cgroup_procs}" ]; then
        cgroup_procs="${cgroup_procs}/${SV_CG}/cgroup.procs"
    fi

    if ! [ -f "${cgroup_procs}" ]; then
        return 0
    fi

    while read -r p; do
        [ "$p" -eq $$ ] && continue
        cgroup_pids="${cgroup_pids} ${p}"
    done < "${cgroup_procs}"
    return 0
}

cgroup2_remove() {
    local cgroup_path sv_cgroup_path

    cgroup_path="$(cgroup2_find_path)"

    if [ -z "${cgroup_path}" ]; then
        return 0
    fi

    sv_cgroup_path="${cgroup_path}/${SV_CG}"

    if [ ! -d "${sv_cgroup_path}" ] || [ ! -e "${sv_cgroup_path}"/cgroup.events ]; then
        return 0
    fi

    if grep -qx "$$" "${sv_cgroup_path}/cgroup.procs"; then
        printf "%d" 0 > "${cgroup_path}/cgroup.procs"
    fi

    local key populated value

    while read -r key value; do
        case "${key}" in
            populated) populated=${value} ;;
            *) ;;
        esac
    done < "${sv_cgroup_path}/cgroup.events"

    if [ "${populated}" = 1 ]; then
        return 0
    fi

    rmdir "${sv_cgroup_path}"
    return 0
}

cgroup2_kill_cgroup() {
    local cgroup_path

    cgroup_path="$(cgroup2_find_path)"

    if [ -z "${cgroup_path}" ]; then
        return 1
    fi
    sv_cgroup_path="${cgroup_path}/${SV_CG}"

    if [ -f "${sv_cgroup_path}"/cgroup.kill ]; then
        printf "%d" 1 > "${sv_cgroup_path}"/cgroup.kill
    fi
    return
}

cgroup2_fallback_cleanup() {
    local loops=0
    cgroup2_get_pids
    if [ -n "${cgroup_pids}" ]; then
        kill -s CONT ${cgroup_pids} 2> /dev/null
        kill -s "${stopsig:-TERM}" ${cgroup_pids} 2> /dev/null
        if "${SV_CG_SEND_SIGHUP}"; then
            kill -s HUP ${cgroup_pids} 2> /dev/null
        fi
        kill -s "${stopsig:-TERM}" ${cgroup_pids} 2> /dev/null
        cgroup2_get_pids
        while [ -n "${cgroup_pids}" ] &&
            [ "${loops}" -lt "${SV_CG_TIMEOUT_STOPSEC}" ]; do
            loops=$((loops+1))
            sleep 1
            cgroup2_get_pids
        done
        if [ -n "${cgroup_pids}" ] && "${SV_CG_SEND_SIGKILL}"; then
            kill -s KILL ${cgroup_pids} 2> /dev/null
        fi
    fi
}

sv_cgroup2_cleanup() {
    cgroup2_running || return 0
    cgroup2_kill_cgroup || cgroup2_fallback_cleanup
    cgroup2_remove
    cgroup2_get_pids
    if [ -n "${cgroup_pids}" ]; then
        printf "%s\n" "Unable to stop all processes"
    fi
    return 0
}

# }}}
