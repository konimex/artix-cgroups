SHELL=/bin/bash

PREFIX = /usr/local
SCRIPTDIR = $(PREFIX)/lib/artix
LIBDIR = $(PREFIX)/share/artix/cgroups
BUILDDIR = build
CGROUP ?= artix

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BINPROGS_SRC = $(wildcard src/*.in)
BINPROGS = $(addprefix $(BUILDDIR)/,$(patsubst src/%,bin/%,$(patsubst %.in,%,$(BINPROGS_SRC))))
LIBRARY_SRC = $(call rwildcard,src/lib,*.sh)
LIBRARY = $(addprefix $(BUILDDIR)/,$(patsubst src/%,%,$(patsubst %.in,%,$(LIBRARY_SRC))))


all: binprogs library
binprogs: $(BINPROGS)
library: $(LIBRARY)

edit = sed -e "s|@libdir[@]|$(LIBDIR)|g"\
	-e "s|@cgname[@]|$(CGROUP)|"

GEN_MSG = @echo "GEN $(patsubst $(BUILDDIR)/%,%,$@)"

RM = rm -f

define buildInScript
$(1)/%: $(2)%$(3)
	$$(GEN_MSG)
	@mkdir -p $$(dir $$@)
	@$(RM) "$$@"
	@cat $$< | $(edit) >$$@
	@chmod $(4) "$$@"
	@bash -O extglob -n "$$@"
endef

$(eval $(call buildInScript,build/bin,src/,.in,755))
$(eval $(call buildInScript,build/lib,src/lib/,,644))


clean:
	rm -rf $(BUILDDIR)

install: all
	install -dm0755 $(DESTDIR)$(SCRIPTDIR)
	install -m0755 $(BINPROGS) $(DESTDIR)$(SCRIPTDIR)
	install -dm0755 $(DESTDIR)$(LIBDIR)
	cp -ra $(BUILDDIR)/lib/* $(DESTDIR)$(LIBDIR)

uninstall:
	for f in $(notdir $(LIBRARY)); do rm -f $(DESTDIR)$(LIBDIR)/$$f; done
	rmdir --ignore-fail-on-non-empty \
		$(DESTDIR)$(LIBDIR)

dist:
	git archive --format=tar --prefix=$(TOOLS)-$(V)/ v$(V) | gzip > $(TOOLS)-$(V).tar.gz
	gpg --detach-sign --use-agent $(TOOLS)-$(V).tar.gz

check: $(BIN_CG_SRC) $(LIB_CG_SRC)
	shellcheck -x $^

.PHONY: all binprogs library clean install uninstall dist check
.DELETE_ON_ERROR:
